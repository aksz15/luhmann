{
  "notes": {
    "editor_note": [
      "<span><span class=\"note\"><\/span><\/span>"
    ],
    "editor_link": "<span><span class=\"note\"><\/span><\/span>",
    "header_editor": "<span><span class=\"note\"><\/span><\/span>"
  },
  "meta": {
    "ordnungsnummer": "32",
    "zettel_type": "Notizzettel",
    "logical_zettelkasten": "1",
    "scan-in-drawer": "00583",
    "page_type": "Zettelvorderseite",
    "abteilung": "Normaler Bestand"
  },
  "flags": {
    "hasBranchVisualization": true,
    "hasCorrectedImage": false
  },
  "ekin": "ZK_1_NB_32-12b5_V",
  "auszug": "03",
  "transcription": {
    "isTranscribed": true,
    "html": "<div>\n\t\t\t\t\n\t\t\t\t<p><span class=\"zettel-nummer\">32,12b5<\/span> Objekt oder Gegenstand der Erkenntnis. Es ist wirklich zweierlei,<br>zu fragen, ob die Wahrheit unserer Erkenntnis objektiv, gegenständlich,<br>also nicht bloss subjektiv sei, oder zu fragen, ob die Wahrheit das Objekt,<br>den Gegenstand unserer Erkenntnis bilde, und zwar entweder so<span>-<\/span><br>gar das id quod oder wenigstens das id in quo. Die zweite Frage ist die<br>Frage nach der Idealität oder Realität des Erkenntnisgegenstandes;<br>die erste [<span class=\"addition\" title=\"Hinzufügung NL am Rand links\">S. 233<\/span>] Frage dagegen hält sich diesseits von Idealismus und Realis<span>-<\/span><br>mus, indem sie zunächst nur die Erkenntniswahrheit sichert, und<br>die Frage nach der Seinswahrheit oder der Wahrheit<span class=\"add_editor\" title=\"[Hinzufügung des NL-Archivs]\">en<\/span><span class=\"note_editor\" title=\"NL notiert hier abweichend vom Original &#34;Wahrheit&#34; statt &#34;Wahrheiten&#34;. [Anmerkung des NL-Archivs]\">[*]<\/span> im Sein, der<br>idealen Struktur des Seienden offen lässt.\"<\/p>\n\t\t\t\t<p>Die Verlagerung der Wahrheit in das erkennende Bewusstsein<br>besagt also keineswegs schon, dass auch die Geltungskriterien der<br>Wahrheit dem Bewusstsein entnommen werden; sie legt nur den<br>ersten Grund dafür.<\/p>\n\t\t\t\t<p>Zur Seinsgrundlage der scholastischen Wahrheit vgl. <span class=\"bibl\"><a href=\"\/bestand\/bibliographie\/item\/soehngen_1930_sein\" title=\"soehngen_1930_sein\">Söhngen, S. 116ff., 257ff.<\/a><\/span>,<br>ferner die Zitate <a href=\"\/bestand\/zettelkasten\/zettel\/ZK_1_NB_32-12b2a_V\" class=\"vw_einzel_entf\" title=\"ZK_1_NB_32-12b2a_V\">32,12b2a<\/a>; <a href=\"\/bestand\/zettelkasten\/zettel\/ZK_1_NB_32-12b5a_V\" class=\"vw_einzel_nah\" title=\"ZK_1_NB_32-12b5a_V\"><span class=\"red\">1<\/span><\/a>. Ausdrücklich behandelt ist diese Frage unter <a href=\"\/bestand\/zettelkasten\/zettel\/ZK_1_NB_32-12b6_V\" class=\"join\" title=\"Fortsetzung des Textes auf Zettel ZK_1_NB_32-12b6_V\"><img src=\"\/assets\/images\/nav_icons\/arrow-right-bold-box.svg\"><\/a><\/p>\n\t\t\t<\/div>"
  },
  "index": {
    "editor_I": "#ZK_1_editor_I_32-12"
  },
  "navigation": {
    "original_physical": [
      {
        "ekin": "ZK_1_NB_32-12b5a_V",
        "luhmann_number": "32-12b5a",
        "title": "ZK I: Zettel 32,12b5a",
        "relation": "naechste-vorderseite-im-zettelkasten",
        "image_id": "ZK_1_03_06_085_V_N_NB_32-12b5a"
      },
      {
        "ekin": "ZK_1_NB_32-12b4_V",
        "luhmann_number": "32-12b4",
        "title": "ZK I: Zettel 32,12b4",
        "relation": "vorherige-vorderseite-im-zettelkasten",
        "image_id": "ZK_1_03_06_081_V_N_NB_32-12b4"
      },
      {
        "ekin": "ZK_1_NB_32-12b5_R",
        "luhmann_number": "32-12b5",
        "title": "",
        "relation": "naechster-scan-im-zettelkasten",
        "image_id": "ZK_1_03_06_084_R_D_NB_32-12b5"
      },
      {
        "ekin": "ZK_1_NB_32-12b4_R",
        "luhmann_number": "32-12b4",
        "title": "",
        "relation": "vorheriger-scan-im-zettelkasten",
        "image_id": "ZK_1_03_06_082_R_D_NB_32-12b4"
      },
      {
        "ekin": "ZK_1_NB_32-12b5_R",
        "luhmann_number": "32-12b5",
        "title": "",
        "relation": "kehrseite",
        "image_id": "ZK_1_03_06_084_R_D_NB_32-12b5"
      }
    ],
    "logical": [
      {
        "ekin": "ZK_1_NB_32-12b6_V",
        "luhmann_number": "32-12b6",
        "title": "ZK I: Zettel 32,12b6",
        "relation": "vorwaerts-in-diesem-Strang",
        "image_id": "ZK_1_03_06_087_V_N_NB_32-12b6"
      },
      {
        "ekin": "ZK_1_NB_32-12b5a_V",
        "luhmann_number": "32-12b5a",
        "title": "ZK I: Zettel 32,12b5a",
        "relation": "vorwaerts-in-ergaenzendem-Strang",
        "image_id": "ZK_1_03_06_085_V_N_NB_32-12b5a"
      },
      {
        "ekin": "ZK_1_NB_32-12b4_V",
        "luhmann_number": "32-12b4",
        "title": "ZK I: Zettel 32,12b4",
        "relation": "rueckwaerts-in-diesem-Strang",
        "image_id": "ZK_1_03_06_081_V_N_NB_32-12b4"
      },
      {
        "ekin": "ZK_1_NB_32-12b1_V",
        "luhmann_number": "32-12b1",
        "title": "ZK I: Zettel 32,12b1",
        "relation": "Anfang-des-Strangs",
        "image_id": "ZK_1_03_06_073_V_N_NB_32-12b1"
      },
      {
        "ekin": "ZK_1_NB_32-12b_V",
        "luhmann_number": "32-12b",
        "title": "ZK I: Zettel 32,12b",
        "relation": "zurueck-zu-hoeherem-Strang",
        "image_id": "ZK_1_03_06_071_V_N_NB_32-12b"
      },
      {
        "ekin": "ZK_1_NB_32-17_V",
        "luhmann_number": "32-17",
        "title": "ZK I: Zettel 32,17",
        "relation": "Anfang-des-naechsten-Komplexes",
        "image_id": "ZK_1_03_07_053_V_N_NB_32-17"
      },
      {
        "ekin": "ZK_1_NB_32-11_V",
        "luhmann_number": "32-11",
        "title": "ZK I: Zettel 32,11",
        "relation": "Anfang-des-vorherigen-Komplexes",
        "image_id": "ZK_1_03_06_015_V_N_NB_32-11"
      },
      {
        "ekin": "ZK_1_NB_32-12_V",
        "luhmann_number": "32-12",
        "title": "ZK I: Zettel 32,12",
        "relation": "Zum-Einstiegspunkt",
        "image_id": "ZK_1_03_06_053_V_N_NB_32-12"
      }
    ],
    "corrected_physical": [
      {
        "ekin": "ZK_1_NB_32-12b4_V",
        "luhmann_number": "32-12b4",
        "title": "ZK I: Zettel 32,12b4",
        "relation": "Vorderseite-des-vorherigen-Zettels-in-corrected-physical-sequence",
        "image_id": "ZK_1_03_06_081_V_N_NB_32-12b4"
      },
      {
        "ekin": "ZK_1_NB_32-12b5a_V",
        "luhmann_number": "32-12b5a",
        "title": "ZK I: Zettel 32,12b5a",
        "relation": "Vorderseite-des-naechsten-Zettels-in-corrected-physical-sequence",
        "image_id": "ZK_1_03_06_085_V_N_NB_32-12b5a"
      }
    ],
    "numerical": [
      {
        "ekin": "ZK_1_NB_32-12b6_V",
        "luhmann_number": "32-12b6",
        "title": "ZK I: Zettel 32,12b6",
        "relation": "vorwaerts-im-nummerierten-Gliederungsverlauf"
      },
      {
        "ekin": "ZK_1_NB_32-12b4_V",
        "luhmann_number": "32-12b4",
        "title": "ZK I: Zettel 32,12b4",
        "relation": "rueckwaerts-im-nummerierten-Gliederungsverlauf"
      }
    ]
  },
  "is_front": true,
  "luhmann_number": "32-12b5",
  "zk_number": "1",
  "subtitle": "",
  "title": "ZK I: Zettel 32,12b5",
  "availability": "free",
  "image": {
    "change": "rotate_0",
    "id": "ZK_1_03_06_083_V_N_NB_32-12b5"
  },
  "file": {
    "filename": "ZK_1_03_06_083_V_N_NB_32-12b5.xml",
    "dir-auszug": "03",
    "dir-zettelkasten": "1",
    "dir-batch": "06",
    "id": "ZK_1_03_06_083_V_N_NB_32-12b5"
  },
  "shortTitle": "32,12b5",
  "isDummy": false
}