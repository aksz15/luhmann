{
  "notes": {
    "editor_note": [
      "<span><span class=\"note\"><\/span><\/span>"
    ],
    "editor_link": "<span><span class=\"note\"><\/span><\/span>",
    "header_editor": "<span><span class=\"note\"><\/span><\/span>"
  },
  "meta": {
    "ordnungsnummer": "17",
    "zettel_type": "Notizzettel",
    "logical_zettelkasten": "1",
    "scan-in-drawer": "04393",
    "page_type": "Zettelvorderseite",
    "abteilung": "Normaler Bestand"
  },
  "flags": {
    "hasBranchVisualization": true,
    "hasCorrectedImage": false
  },
  "ekin": "ZK_1_NB_17-1b12f_V",
  "auszug": "01",
  "transcription": {
    "isTranscribed": true,
    "html": "<div>\n\n<p><span class=\"zettel-nummer\">17,1b12f<\/span> Dieser Situationsbegriff steht also aller mechanistischen<br>Psychologie konträr entgegen, und setzt grundsätzlich die<br>Freiheit menschlicher Sinngebung voraus. Auf anderer Basis<br>ist denn auch eine Lehre vom rationalen Entscheiden undenkbar.<br>\"les choses … ne peuvent exercer une influence sur nous<br>que par leur sens, et … ce sens est constitué par nous\"<span class=\"note_editor\" title=\"Die von NL im Zitat notierten Auslassungen korrespondieren nicht dem Original: &#34;les choses ne peuvent nous importer ou (si l'on ose se servir de cette expression) exercer une influence sur nous que par leur sens, et que ce sens est constitué par nous.&#34; [Anmerkung des NL-Archivs]\">[*]<\/span><br>(<span class=\"bibl\"><a href=\"\/bestand\/bibliographie\/item\/waelhens_1951_philosophie\" title=\"waelhens_1951_philosophie\">de Waelhens, S. 317\/18<\/a><\/span>). Dass damit nicht etwa absolute Freiheit<br>beliebiger Sinnstiftung vorausgesetzt ist, haben die glänzenden<br>Ausführungen <span class=\"bibl\"><a href=\"\/bestand\/bibliographie\/item\/merleau-ponty_1945_perception\" title=\"merleau-ponty_1945_perception\">Merleau-Pontys, S. 499ff.<\/a><\/span> dargelegt. Die Sinnge<span>-<\/span><br>bung vollzieht sich durch den mit seinem Leib faktisch in dieser<br>Welt existierenden Menschen, der bestimmte Strukturen der<br>Einordnung in die Welt als immer schon vorgegeben vorfindet<br>und akzeptieren muss. Auf diesem Boden erst wächst seine<br>faktische Freiheit. In diesem Sinne ist jede Situation gebildet<br>aus Bindung und Freiheit. Und dem entspricht, dass Freiheit <a href=\"\/bestand\/zettelkasten\/zettel\/ZK_1_NB_17-1b12g_V\" class=\"join\" title=\"Fortsetzung des Textes auf Zettel ZK_1_NB_17-1b12g_V\"><img src=\"\/assets\/images\/nav_icons\/arrow-right-bold-box.svg\"><\/a><\/p>\n<\/div>"
  },
  "index": {
    "editor_I": "#ZK_1_editor_I_17-1b12"
  },
  "navigation": {
    "original_physical": [
      {
        "ekin": "ZK_1_NB_17-1b12g_V",
        "luhmann_number": "17-1b12g",
        "title": "ZK I: Zettel 17,1b12g",
        "relation": "naechste-vorderseite-im-zettelkasten",
        "image_id": "ZK_1_01_44_095_V_N_NB_17-1b12g"
      },
      {
        "ekin": "ZK_1_NB_17-1b12e1_V",
        "luhmann_number": "17-1b12e1",
        "title": "ZK I: Zettel 17,1b12e1",
        "relation": "vorherige-vorderseite-im-zettelkasten",
        "image_id": "ZK_1_01_44_091_V_N_NB_17-1b12e1"
      },
      {
        "ekin": "ZK_1_NB_17-1b12f_R",
        "luhmann_number": "17-1b12f",
        "title": "",
        "relation": "naechster-scan-im-zettelkasten",
        "image_id": "ZK_1_01_44_094_R_D_NB_17-1b12f"
      },
      {
        "ekin": "ZK_1_NB_17-1b12e1_R",
        "luhmann_number": "17-1b12e1",
        "title": "",
        "relation": "vorheriger-scan-im-zettelkasten",
        "image_id": "ZK_1_01_44_092_R_D_NB_17-1b12e1"
      },
      {
        "ekin": "ZK_1_NB_17-1b12f_R",
        "luhmann_number": "17-1b12f",
        "title": "",
        "relation": "kehrseite",
        "image_id": "ZK_1_01_44_094_R_D_NB_17-1b12f"
      }
    ],
    "logical": [
      {
        "ekin": "ZK_1_NB_17-1b12g_V",
        "luhmann_number": "17-1b12g",
        "title": "ZK I: Zettel 17,1b12g",
        "relation": "vorwaerts-in-diesem-Strang",
        "image_id": "ZK_1_01_44_095_V_N_NB_17-1b12g"
      },
      {
        "ekin": "ZK_1_NB_17-1b12e_V",
        "luhmann_number": "17-1b12e",
        "title": "ZK I: Zettel 17,1b12e",
        "relation": "rueckwaerts-in-diesem-Strang",
        "image_id": "ZK_1_01_44_089_V_N_NB_17-1b12e"
      },
      {
        "ekin": "ZK_1_NB_17-1b12_V",
        "luhmann_number": "17-1b12",
        "title": "ZK I: Zettel 17,1b12",
        "relation": "Anfang-des-Strangs",
        "image_id": "ZK_1_01_44_001_V_N_NB_17-1b12"
      },
      {
        "ekin": "ZK_1_NB_17-2_V",
        "luhmann_number": "17-2",
        "title": "ZK I: Zettel 17,2",
        "relation": "Anfang-des-naechsten-Komplexes",
        "image_id": "ZK_1_01_48_061_V_N_NB_17-2"
      },
      {
        "ekin": "ZK_1_NB_17_2_V",
        "luhmann_number": "17",
        "title": "ZK I: Zettel 17 (2)",
        "relation": "Anfang-des-vorherigen-Komplexes",
        "image_id": "ZK_1_01_41_055_V_N_NB_17_2"
      },
      {
        "ekin": "ZK_1_NB_17-1b12_V",
        "luhmann_number": "17-1b12",
        "title": "ZK I: Zettel 17,1b12",
        "relation": "Zum-Einstiegspunkt",
        "image_id": "ZK_1_01_44_001_V_N_NB_17-1b12"
      }
    ],
    "corrected_physical": [
      {
        "ekin": "ZK_1_NB_17-1b12e1_V",
        "luhmann_number": "17-1b12e1",
        "title": "ZK I: Zettel 17,1b12e1",
        "relation": "Vorderseite-des-vorherigen-Zettels-in-corrected-physical-sequence",
        "image_id": "ZK_1_01_44_091_V_N_NB_17-1b12e1"
      },
      {
        "ekin": "ZK_1_NB_17-1b12g_V",
        "luhmann_number": "17-1b12g",
        "title": "ZK I: Zettel 17,1b12g",
        "relation": "Vorderseite-des-naechsten-Zettels-in-corrected-physical-sequence",
        "image_id": "ZK_1_01_44_095_V_N_NB_17-1b12g"
      }
    ],
    "numerical": [
      {
        "ekin": "ZK_1_NB_17-1b12g_V",
        "luhmann_number": "17-1b12g",
        "title": "ZK I: Zettel 17,1b12g",
        "relation": "vorwaerts-im-nummerierten-Gliederungsverlauf"
      },
      {
        "ekin": "ZK_1_NB_17-1b12e_V",
        "luhmann_number": "17-1b12e",
        "title": "ZK I: Zettel 17,1b12e",
        "relation": "rueckwaerts-im-nummerierten-Gliederungsverlauf"
      }
    ]
  },
  "is_front": true,
  "luhmann_number": "17-1b12f",
  "zk_number": "1",
  "subtitle": "",
  "title": "ZK I: Zettel 17,1b12f",
  "availability": "free",
  "image": {
    "change": "rotate_0",
    "id": "ZK_1_01_44_093_V_N_NB_17-1b12f"
  },
  "file": {
    "filename": "ZK_1_01_44_093_V_N_NB_17-1b12f.xml",
    "dir-auszug": "01",
    "dir-zettelkasten": "1",
    "dir-batch": "44",
    "id": "ZK_1_01_44_093_V_N_NB_17-1b12f"
  },
  "shortTitle": "17,1b12f",
  "isDummy": false
}